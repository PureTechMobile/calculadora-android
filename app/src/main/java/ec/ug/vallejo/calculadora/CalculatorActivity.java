package ec.ug.vallejo.calculadora;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CalculatorActivity extends AppCompatActivity implements View.OnClickListener {

    protected TextView display;
    protected TextView display2;

    protected String inputString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        this.display = this.findViewById(R.id.display);
        this.display2 = this.findViewById(R.id.display2);
        final Button button9 = this.findViewById(R.id.button9);
        final Button button8 = this.findViewById(R.id.button8);
        final Button button7 = this.findViewById(R.id.button7);
        final Button button6 = this.findViewById(R.id.button6);
        final Button button5 = this.findViewById(R.id.button5);
        final Button button4 = this.findViewById(R.id.button4);
        final Button button3 = this.findViewById(R.id.button3);
        final Button button2 = this.findViewById(R.id.button2);
        final Button button1 = this.findViewById(R.id.button1);
        final Button button0 = this.findViewById(R.id.button0);
        final Button button_divide = this.findViewById(R.id.button_divide);
        final Button button_multiply = this.findViewById(R.id.button_multiply);
        final Button button_plus = this.findViewById(R.id.button_plus);
        final Button button_substract = this.findViewById(R.id.button_substract);
        final Button button_dot = this.findViewById(R.id.button_ce);
        final Button button_equals = this.findViewById(R.id.button_equals);
        final Button buttonpunto = this.findViewById(R.id.punto);

        button9.setOnClickListener(this);
        button8.setOnClickListener(this);
        button7.setOnClickListener(this);
        button6.setOnClickListener(this);
        button5.setOnClickListener(this);
        button4.setOnClickListener(this);
        button3.setOnClickListener(this);
        button2.setOnClickListener(this);
        button1.setOnClickListener(this);
        button0.setOnClickListener(this);
        button_divide.setOnClickListener(this);
        button_multiply.setOnClickListener(this);
        button_plus.setOnClickListener(this);
        button_substract.setOnClickListener(this);
        button_dot.setOnClickListener(this);
        button_equals.setOnClickListener(this);
        buttonpunto.setOnClickListener(this);

        LinearLayout copiar=this.findViewById(R.id.copiar_resultado);
        copiar.setOnClickListener(this);

        LinearLayout borrar=this.findViewById(R.id.borrar);
        borrar.setOnClickListener(this);

        LinearLayout salir=this.findViewById(R.id.salir);
        salir.setOnClickListener(this);

        borrar.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                inputString="";
                display2.setText(inputString);
                return true;
            }
        });

        inputString = "";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button9:
                this.updateDisplay2("9");
                inputString += "9";
                break;
                case R.id.punto:
                this.updateDisplay2(".");
                inputString += ".";
                break;
            case R.id.button8:
                this.updateDisplay2("8");
                inputString += "8";
                break;
            case R.id.button7:
                this.updateDisplay2("7");
                inputString += "7";
                break;
            case R.id.button6:
                this.updateDisplay2("6");
                inputString += "6";
                break;
            case R.id.button5:
                this.updateDisplay2("5");
                inputString += "5";
                break;
            case R.id.button4:
                this.updateDisplay2("4");
                inputString += "4";
                break;
            case R.id.button3:
                this.updateDisplay2("3");
                inputString += "3";
                break;
            case R.id.button2:
                this.updateDisplay2("2");
                inputString += "2";
                break;
            case R.id.button1:
                this.updateDisplay2("1");
                inputString += "1";
                break;
            case R.id.button0:
                this.updateDisplay2("0");
                inputString += "0";
                break;
            case R.id.button_divide:
                this.updateDisplay2(" / ");
                inputString += " / ";
                break;
            case R.id.button_multiply:
                this.updateDisplay2(" * ");
                inputString += " * ";
                break;
            case R.id.button_plus:
                this.updateDisplay2(" + ");
                inputString += " + ";
                break;
            case R.id.button_substract:
                this.updateDisplay2(" - ");
                inputString += " - ";
                break;
            case R.id.button_ce:
                this.display.setText("");
                this.display2.setText("");
                inputString = "";
                break;
                case R.id.salir:
                onBackPressed();
                break;
            case R.id.button_equals:
                if(inputString.contains("  "))
                {
                    Toast.makeText(getApplicationContext(),"Error de sintaxis", Toast.LENGTH_LONG).show();
                    return;
                }else
                {
                    display2.setText("");
                    prepareInput(inputString);
                    inputString = "";
                }

                break;
                case R.id.borrar:
                    try {
                        if(inputString.length()>0)
                        {
                            if(inputString.length()>2)
                            {
                                if(inputString.substring(inputString.length()-1,inputString.length()).equals(" "))
                                {
                                    inputString=inputString.substring(0,inputString.length()-3);
                                    display2.setText(inputString);
                                }else {
                                    inputString=inputString.substring(0,inputString.length()-1);
                                    display2.setText(inputString);
                                }

                            }else {
                                inputString=inputString.substring(0,inputString.length()-1);
                                display2.setText(inputString);
                            }

                        }
                    }catch (Exception e)
                    {
                        Toast.makeText(getApplicationContext(),"Error "+e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                    break;
                case R.id.copiar_resultado:
                senddataforResult();
                break;
        }

    }
    void senddataforResult()
    {
        String dato=display.getText().toString().trim();
        if(dato.equals(""))
        {
            Toast.makeText(getApplicationContext(),"No hay resultado calculado", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("text",  dato);
            clipboard.setPrimaryClip(clip);
        }catch (Exception e)
        {

        }

        Intent returnIntent = new Intent();
        returnIntent.putExtra("RESULTADO",dato);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();


    }

    private void updateDisplay(String value) {
        this.display.setText(value);
    }

    private void updateDisplay2(final String value) {
        this.display2.append(value);
    }

    private void prepareInput(String input) {


        if(!display.getText().toString().trim().equals("")&&input.startsWith(" "))
        {
            input=display.getText().toString().trim()+input;
        }

        while (input.startsWith(" "))
        {
            input=input.substring(3, input.length());
        }

        while (input.endsWith(" "))
        {
            input=input.substring(0, input.length()-3);
        }

        calculate(input);
    }

    private void calculate(String input) {
        String[] arr = input.split(" ");
        if(arr.length > 1) {
            for(int i=0; i<arr.length; i++) {
                double temp = 0;
                try {
                    if(arr[i].contains("/")) {
                        double op1 = Double.parseDouble(arr[i-1]);
                        double op2 = Double.parseDouble(arr[i+1]);
                        if(op2 == 0.0) {
                            System.out.println("Not allowed");
                            updateDisplay("Error");
                            input = "";
                            return;
                        }
                        temp = op1 / op2;
                        arr[i-1] = " ";
                        arr[i] = String.valueOf(temp);
                        arr[i+1] = " ";
                    }
                    if(arr[i].contains("*")) {
                        double op1 =  Double.parseDouble(arr[i-1]);
                        double op2 =  Double.parseDouble(arr[i+1]);
                        temp = op1 * op2;
                        arr[i-1] = " ";
                        arr[i] = String.valueOf(temp);
                        arr[i+1] = " ";
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            for(int i=0; i<arr.length; i++) {
                double temp = 0;
                try {
                    if(arr[i].contains("+")) {
                        double op1 =  Double.parseDouble(arr[i-1]);
                        double op2 =  Double.parseDouble(arr[i+1]);
                        temp = op1 + op2;
                        arr[i-1] = " ";
                        arr[i] = String.valueOf(temp);
                        arr[i+1] = " ";
                    }
                    if(arr[i].contains("-")) {
                        double op1 =  Double.parseDouble(arr[i-1]);
                        double op2 =  Double.parseDouble(arr[i+1]);
                        temp = op1 - op2;
                        arr[i-1] = " ";
                        arr[i] = String.valueOf(temp);
                        arr[i+1] = " ";
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            inputString = "";
            for(String s : arr) {
                if(s != " ") {
                    inputString += s;
                    inputString += " ";
                }
            }
            calculate(inputString);
        } else {
            updateDisplay(arr[0]);
        }
    }


    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }
}